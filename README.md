# Domain Event

The base `domain_event_broadcast` function. Note that each event is required to have a `ref_type` and `ref_id` which loosely correspond to `entity_type` and `entity_id`. Currently users and nodes will have events tied to them show up in their `Domain Events` tab.

```php
$new_event = array(
  'type' => 'yelp_account_linked',
  'data' => array(
    'yelp_url' => $yelp_url,
    'client_nid' => $client_node->nid,
    'nid_ref' => $node->nid,
    'return'=>$return
  ),
);
domain_event_broadcast($new_event, 'node', $node->nid);
```


## Context

Sometimes you might trigger Domain Events from system hooks where you want to know the context from which they are broadcasting. For example, you could hook into `hook_entity_insert` but want to know whether the `node_save` was called from a node form or maybe some larger cron process.

You can stack context and the broadcast will save the chain.

```php
function example_node_form_submit($form, &$form_state) {
  domain_event_context_push($form['#id']);
  code_that_triggers_domain_events();
  domain_event_context_pop();
}
```
