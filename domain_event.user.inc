<?php
/*
 *
 */
function domain_event_user_event($type, $user, $data, $timestamp=NULL) {
  $event = array(
    'ref_type' => 'user',
    'ref_id' => $user->uid,
    'type' => $type,
    'data' => $data,
  );
  if(!empty($timestamp)) {
    $event['timestamp'] = $timestamp;
  }
  domain_event_broadcast($event);
}

/*
 * hook_user_update
 */
function domain_event_user_update(&$edit, $account, $category) {
  $bases = array('mail', 'status', 'name');
  $original = $edit['original'];
  $diff = entity_field_diff($edit, $original, 'user', 'user', $bases);
  $diff = $diff['diff'];
  $array_diff = array_diff_key($edit['roles'], $original->roles);
  if (!empty($array_diff) ||
      array_diff_key($original->roles, $edit['roles'])) {
      $diff['roles'] = array('old' => $original->roles, 'value' => $edit['roles']);
  };
  domain_event_user_event('user_update', $account, array('diff' => $diff));
}

/*
 * hook_user_insert
 */
function domain_event_user_insert(&$edit, $account, $category) {
  domain_event_user_event('user_insert', $account, array('user' => $account));
}

/*
 * hook_user_login
 */
function domain_event_user_login(&$edit, $account) {
  domain_event_user_event('user_login', $account, array('login' => $account->login));
}

/*
 * Domain Events tab for users
 */
function domain_event_user_page_access($account) {
  return user_access('view domain event') && user_edit_access($account);
}

function domain_event_user_page($user) {
  $domain_events = domain_event_by_ref('user', $user->uid);
  return domain_event_listing_table($domain_events);
}
