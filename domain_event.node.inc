<?php

/*
 * Using entity_update to get run after node_update.
 */
function domain_event_entity_update($node, $type) {
  if($type != 'node') {
    return;
  }

  $diff = domain_event_node_diff($node, $node->original);
  domain_event_node_event('node_update', $node, array('diff' => $diff), $node->changed);
}

function domain_event_entity_insert($node, $type) {
  if($type != 'node') {
    return;
  }

  domain_event_node_event('node_insert', $node, array('node' => $node), $node->changed);
}

function domain_event_entity_delete($node, $type) {
  if($type != 'node') {
    return;
  }

  domain_event_node_event('node_delete', $node, array('node' => $node), REQUEST_TIME);
}

/*
 * Will show the fields that changed between two versions of
 * a node. It will show some node fields and all the fields.
 * If someone attaches somethign to a node without using field api
 * this won't catch it.
 */
function domain_event_node_diff($new, $old) {
  $old = (array) $old;
  $new = (array) $new;

  $node_fields = array(
    'title',
    'changed',
    'created',
    'status',
  );

  $res = entity_field_diff($new, $old, 'node', $new['type'], $node_fields);
  $diff = $res['diff'];
  $skip = $res['processed'];
  $skip[] = 'log';

  $also = array('publish_on', 'unpublish_on');
  $merged_keys = array_unique(array_merge(array_keys($old), $also));

  foreach ($merged_keys as $key) {
    if(in_array($key, $skip, TRUE)) {
      continue;
    }

    $change = domain_event_diff_single($old, $new, $key);
    if (!empty($change)) {
      $diff[$key] = $change;
    }
  }
  return $diff;
}


/*
 * 
 */
function domain_event_node_event($type, $node, $data, $timestamp=NULL) {
  if (variable_get('domain_event_skip_node', TRUE)) {
    return;
  }

  if (is_null($timestamp)) {
    $timestamp = $node->changed;
  }
  $event = array(
    'ref_type' => 'node',
    'ref_id' => $node->nid,
    'ref_subtype' => $node->type,
    'type' => $type,
    'data' => $data,
    'timestamp' => $timestamp,
  );
  domain_event_broadcast($event);
}
