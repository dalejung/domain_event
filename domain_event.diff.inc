<?php

function entity_field_diff($new, $old, $entity_type, $entity_bundle, $bases=array()) {
  $old = (array) $old;
  $new = (array) $new;

  $skip = array();
  $diff = array();

  // this is for non field uh fields.
  // usually the structure is straight forward.
  foreach($bases as $key) {
    $skip[] = $key;
    $change = domain_event_diff_single($old, $new, $key, FALSE);
    if (!empty($change)) {
      $diff[$key] = $change;
    }
  }
  $fields_info = field_info_instances($entity_type, $entity_bundle);

  foreach ($fields_info as $field_name => $value) {
    $field_info = field_info_field($field_name);
    $skip[] = $field_name;

    // special case for files. the new will be empty unless changed
    if(in_array($field_info['type'], array('file', 'image'))) {
      $old_fid = $old[$field_name][LANGUAGE_NONE][0]['fid'];
      $new_fid = $new[$field_name][LANGUAGE_NONE][0]['fid'];

      if($old_fid != $new_fid) {
        $diff[$field_name] = array('old' => $old[$field_name][LANGUAGE_NONE][0],
                                   'value' => file_load($new_fid));
      }
      continue;
    }

    $change = domain_event_diff_single($old, $new, $field_name);
    if (!empty($change)) {
      $diff[$field_name] = $change;
    }
  }
  return array('diff' => $diff, 'processed' => $skip);
}

function array_diff_recursive($left, $right) {
  $diff = array();

  foreach ($left as $key => $lval) {
    $change = domain_event_diff_single($left, $right, $key);
    if (!empty($change)) {
      $diff[$key] = $change['value'];
    }
  }
  return $diff;
}

/*
 * Some fields will either be missing, or have an empty sentinel value.
 * This is not due to explicitly changing the value but due to how drupal
 * handles it's not loading. Something like 'title' might not show up because
 * the url field doesn't expose it, but node_load might fill it with a default value.
 *
 * So the rules are simply:
 * Missing == NULL == "" == a:0:{}
 */
function domain_event_same_empty($left, $right, $key) {
  $l = domain_event_is_empty($left, $key);
  $r = domain_event_is_empty($right, $key);
  return $l and $r;
}

function domain_event_is_empty($arr, $key) {
  if (array_key_exists($key, $arr)) {
    return TRUE;
  }

  if (isset($arr[$key]) && $arr[$key] == " a:0:{}") {
    return TRUE;
  }

  if(array_key_exists($key, $arr) && is_null($arr[$key])) {
    return TRUE;
  }

  return empty($arr[$key]);
}

/*
 * note we return an array if we have a change.
 * this is so NULL signifies no change.
 *
 * Note: empty_check should only be true for cck fields
 */
function domain_event_diff_single($left, $right, $key, $empty_check=TRUE) {
  if (!isset($left[$key])) {
    return false;
  }

  $lval = $left[$key];

  if ($empty_check and in_array($key, array('attributes', 'format', 'safe_value', 'title'), TRUE)) {
    // these keys have looser empty-like equality
    if(domain_event_same_empty($left, $right, $key)) {
      return NULL;
    }
  }
  if (!array_key_exists($key, $right) and !is_null($lval)) {
    return array('old' => $lval, 'value' => NULL);
  }

  $rval = $right[$key];
  // if only 1 is an array, then it is always different
  if ((is_array($lval) + is_array($rval)) == 1) {
    return array('old' => $lval, 'value' => $rval);
  }

  if (is_array($lval) && is_array($rval)) {
    // if left array is empty. no need to recursve.
    if (empty($lval) and isset($rval[LANGUAGE_NONE]) and empty($rval[LANGUAGE_NONE])) {
      return;
    }
    if (empty($lval) and !empty($rval)) {
      $subdiff = $rval;
    } else {
      $subdiff = array_diff_recursive($lval, $rval);
    }

    if (!empty($subdiff)) {
      // Get rid of LANGUAGE_NONE
      if(in_array(LANGUAGE_NONE, array_keys($lval), TRUE) and count($lval) == 1) {
        $subdiff = $subdiff[LANGUAGE_NONE];
        $lval = $lval[LANGUAGE_NONE];
      }
      // so, if we change one value in the array, 'old' will
      // still show the whole old array... yolo
      return array('old' => $lval, 'value' => $subdiff);
    }
    return;
  }
  if ($lval != $rval) {
    return array('old' => $lval, 'value' => $rval);
  }
}
