<?php

function domain_event_by_ref($ref_type, $ref_id) {
  $query = db_select("domain_event", "de")
    ->fields("de")
    ->condition('ref_type', $ref_type)
    ->condition('ref_id', $ref_id)
    ->orderBy('id', 'DESC');
  $results = $query->execute();
  return domain_event_query_load($results);
}

function domain_event_query_load($results) {
  $domain_events = array();
  while($row = $results->fetchAssoc()) {
    $row['data'] = json_decode($row['data'], TRUE);
    $domain_events[] = $row;
  }
  return $domain_events;
}

function domain_event_node_page_access($node) {
  return user_access('view domain event') && node_access('update', $node);
}

function domain_event_node_page($node) {
  $domain_events = domain_event_by_ref('node', $node->nid);
  return domain_event_listing_table($domain_events);
}

function domain_event_listing_table($domain_events) {
  $rows = array();
  $header = array('id', 'user', 'type', 'ref', 'timestamp', 'data summary', 'context', 'location');
  foreach($domain_events as $event) {
    $user = user_load($event['uid']);

    if(!empty($event['data'])) {
      $data_summary = array_keys($event['data']);
      if (in_array($event['type'], array('node_update', 'user_update'))) {
        $data_summary = array_keys($event['data']['diff']);
        foreach($event['data']['additional_diff'] as $diff) {
          $data_summary = array_merge($data_summary, array_keys($diff));
        }
      }
      $data_summary = array_unique($data_summary);
    }

    if($event['ref_type']) {
      $ref = $event['ref_type'].':'.$event['ref_id'];
      if($event['ref_type'] == 'node') {
        $ref = l('nid:'.$event['ref_id'], $event['ref_type'].'/'.$event['ref_id'].'/domain_event');
      }
    }

    $row = array(
      'id' => l($event['id'], 'domain_event/'.$event['id'].'/json'),
      'user' => $user->name,
      'type' => $event['type'],
      'ref_id' => $ref,
      'timestamp' => date('d-M-Y H:i:s T', $event['timestamp']),
      'data_summary' => implode(', ', $data_summary),
      'context' => $event['context'],
      'location' => $event['location'],
    );
    $rows[] = $row;
  }
  $vars = array();
  $vars['rows'] = $rows;
  $vars['header'] = $header;
  $vars['empty'] = "No Domain Events";

  return theme('table', $vars);
}

function domain_event_event_json($domain_event_id) {
  return drupal_json_output(domain_event_load($domain_event_id));
}

function domain_event_listing_page() {
  $results = db_select("domain_event", "de")
    ->fields("de")
    ->orderBy('id', 'DESC')
    ->range(0, 150)
    ->execute()
    ;
  $domain_events = domain_event_query_load($results);
  print(domain_event_listing_table($domain_events));
}
